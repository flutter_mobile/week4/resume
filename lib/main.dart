import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Resume',
    home: Resume(),
  ));
}

class Resume extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Resume'),
      ),
      body: Container(
        child: Center(
          child: Container(
            child: ElevatedButton(
              child: Text(
                'My Resume',
                style: TextStyle(fontSize: 40, color: Colors.black),
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyRsume()),
                );
              },
              style: ElevatedButton.styleFrom(primary: Colors.grey),
            ),
            height: 100,
            width: 300,
          ),
        ),
        color: Colors.yellow[100],
      ),
    );
  }
}

class MyRsume extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Resume"),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                child: ElevatedButton(
                  child: Text(
                    'Profile',
                    style: TextStyle(fontSize: 40, color: Colors.black),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Profile()),
                    );
                  },
                  style: ElevatedButton.styleFrom(primary: Colors.grey[200]),
                ),
                height: 100,
                width: 300,
              ),
              Container(
                child: ElevatedButton(
                  child: Text(
                    'Skill',
                    style: TextStyle(fontSize: 40, color: Colors.black),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Skill()),
                    );
                  },
                  style: ElevatedButton.styleFrom(primary: Colors.red[100]),
                ),
                height: 100,
                width: 300,
              ),
              Container(
                child: ElevatedButton(
                  child: Text(
                    'Education',
                    style: TextStyle(fontSize: 40, color: Colors.black),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Education()),
                    );
                  },
                  style: ElevatedButton.styleFrom(primary: Colors.blue[100]),
                ),
                height: 100,
                width: 300,
              ),
            ],
          ),
        ),
        color: Colors.grey,
      ),
    );
  }
}

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              "images/getstudentimage.jfif",
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "   ชื่อ : ชาคริส กุลวิทย์ดำรงกุล",
                  style: TextStyle(fontSize: 25),
                ),
                Text(
                  "   เพศ : ชาย",
                  style: TextStyle(fontSize: 25),
                ),
                Text(
                  "   วันเกิด : 13 มิ.ย. 2542",
                  style: TextStyle(fontSize: 25),
                ),
                Text(
                  "   อายุ : 22 ปี",
                  style: TextStyle(fontSize: 25),
                ),
                Text(
                  "   ศาสนา : พุทธ",
                  style: TextStyle(fontSize: 25),
                ),
                Text(
                  "   สัญชาติ : ไทย",
                  style: TextStyle(fontSize: 25),
                ),
                Text(
                  "   อีเมล : fluk1459@gmail.com, 61160244@gmail.com",
                  style: TextStyle(fontSize: 25),
                ),
                Text(
                  "   เบอร์โทร์ : 085 - xxxxxxx",
                  style: TextStyle(fontSize: 25),
                ),
              ],
            ),
          ],
        ),
        color: Colors.grey[200],
        padding: EdgeInsets.only(
          top: 20,
        ),
      ),
    );
  }
}

class Skill extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Skill'),
      ),
      body: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          Container(
            child: Row(
              children: [
                Image.asset(
                  "images/html.png",
                  height: 400,
                  width: 500,
                ),
                Image.asset(
                  "images/css.png",
                  height: 400,
                  width: 500,
                ),
                Image.asset(
                  "images/js.png",
                  height: 300,
                  width: 300,
                ),
                Image.asset(
                  "images/vue.png",
                  height: 400,
                  width: 500,
                ),
                Image.asset(
                  "images/flutter.png",
                  height: 400,
                  width: 500,
                ),
              ],
            ),
            color: Colors.red[100],
          ),
        ],
      ),
    );
  }
}

class Education extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Education'),
      ),
      body: ListView(
        children: [
          Container(
            child: Center(
              child: Column(
                children: [
                  Text(
                    "มัธยม",
                    style: TextStyle(fontSize: 60),
                  ),
                  Image.asset(
                    "images/Logowps.jpg",
                    height: 400,
                    width: 200,
                  ),
                  Text(
                    "โรงเรียนวัดป่าประดู่",
                    style: TextStyle(fontSize: 30),
                  ),
                  Text(
                    "\n\nมหาวิทยาลัย",
                    style: TextStyle(fontSize: 60),
                  ),
                  Image.asset(
                    "images/Logo_buu.jpg",
                    height: 400,
                    width: 200,
                  ),
                  Text(
                    "มหาวิทยาลัยบูรพา\nคณะ วิทยาการสารสนเทศ\nสาขา วิทยาการคอมพิวเตอร์",
                    style: TextStyle(
                      fontSize: 30,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            color: Colors.blue[100],
          ),
        ],
      ),
    );
  }
}
